package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;
import java.util.stream.Stream;


import static java.util.stream.Collectors.toList;

public class Subsequence {

    public void printseq(List listx, List listy) {
        System.out.println("List X :");
        for (int i = 0; i < listx.size(); i++)
            System.out.println(" " + listx.get(i));
        System.out.println(" ");
        System.out.println("List Y :");
        for (int i = 0; i < listy.size(); i++)
            System.out.print(" " + listy.get(i));
        System.out.println(" ");
    }

    public boolean checker(List listx, List listy) {
        if (listx == null ||  listy == null) {
            throw  new IllegalArgumentException();
        }
        boolean flag = false;
        int k=0;

        for (int i = 0; i < listx.size(); i++) {
            for (int j = i; j < listy.size(); j++) {
                if (listx.get(i).equals(listy.get(j))) {
					k++;
                    break;
                } else {
                    if (!(listx.get(i).equals(listy.get(j)))) {
						listy.remove(j);
						i--;
						break;
                    }
                }
            }
        }
        if (k!= 0 && listy.size()!=listx.size()) {
            for (int i=k;i<listy.size();i++ ) {
                listy.remove(i);
            }

        }

        if (listx.size() == listy.size() ) {
            flag = true;
        } else {
            flag = false;
        }

        if(listx.size()==0)
            flag=true;

        return flag;

    }


    public static void main(String[] args) {

    }

  public boolean find(List x, List y) {
     return checker(x, y);
   }
}







