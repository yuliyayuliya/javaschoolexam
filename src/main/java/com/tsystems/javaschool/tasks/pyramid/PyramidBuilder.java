package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

import static java.lang.Math.floor;
import static java.lang.Math.sqrt;

public class PyramidBuilder {
    public List<Integer> list;


    public void sort(List<Integer> list) {
        Collections.sort(list);
    }

    public void printsortedmas(List<Integer> list) {
        System.out.println("Отсортированный: ");
        for (int i = 0; i < list.size(); ++i)
            System.out.print(" " + list.get(i));
    }

    public int[][] build(List<Integer> list)  {
        double h = ((-1 + sqrt(1 + 8 * list.size())) / 2);
        System.out.println("");
        int s;
        int f;
        int m = 0;
        int k = 0;

        if (floor(h) == h) {
            int height = (int) h;
            int[][] mas = new int[height][2 * height - 1];

            int elementsListCount = 0;
            int elementsInRow = 2 * height - 1;

            for (int row = 0; row < height; row++) {
                int zeroCount = height - row - 1;
                for (int j = 0; j < zeroCount; j++) {
                    mas[row][j] = 0;
                }
                for (int j = 0; j < row + 1; j++) {
                    mas[row][j * 2 + zeroCount] = list.get(elementsListCount++);
                    if ((j * 2 + zeroCount + 1) < elementsInRow) {
                        mas[row][j * 2 + zeroCount + 1] = 0;
                    }
                }
                for (int j = 0; j < zeroCount - 1; j++) {
                    mas[row][elementsInRow - j - 1] = 0;
                }
            }

            for (int i = 0; i < height; i++) {
                System.out.println("\n");
                for (int j = 0; j < 2 * height - 1; j++) {
                    System.out.print(mas[i][j] + "\t");
                }
            }
            return mas;
        }
        throw new CannotBuildPyramidException();

    }

    public static void main(String[] args) {

    }

    public int[][] buildPyramid (List < Integer > inputNumbers) {
        for (int i = 0; i < inputNumbers.size(); i++) {
            if (inputNumbers.get(i) == null) {
                throw new CannotBuildPyramidException();
            }
        }
        if (inputNumbers.size() == Integer.MAX_VALUE - 1) {
            throw new CannotBuildPyramidException();
        }
        sort(inputNumbers);
        return build(inputNumbers);
    }


}