package com.tsystems.javaschool.tasks.calculator;
import java.util.Collections;
import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;

public class Calculator {

    private final String[] OPERATORS = {"+","-","*","/"};
    private final String SEPARATOR = ",";
    private Stack<String> stackOperations = new Stack<String>();      // стек содержит операторы скобки и функции
    private Stack<String> stackRPN = new Stack<String>();             // стек для выр,преобразованного в обр польскую нотацию
    private Stack<String> stackAnswer = new Stack<String>();          //стек содержит ответ
    private Stack<String> stackRez = new Stack<String>();

    private boolean isSeparator(String token) {
        return token.equals(SEPARATOR);
    }

    private boolean isOperator(String token) {
        for (String item : OPERATORS) {
            if (item.equals(token)) {
                return true;
            }
        }
        return false;
    }

    private boolean isOpenBracket(String token) {
        return token.equals("(");
    }

    private boolean isCloseBracket(String token) {
        return token.equals(")");
    }


    private boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private byte getPrecedence(String token) {
        if (token.equals("+") || token.equals("-")) {
            return 1;
        }
        return 2;
    }

    public boolean checker (String expression){
    boolean f=true;

        for (int i = 0; i <expression.length();i++) {
            if (((expression.charAt(i) == '+') || (expression.charAt(i) == '-')
                    || (expression.charAt(i) == '/') || (expression.charAt(i) == '*')) && ((expression.charAt(i + 1) == '+') || (expression.charAt(i + 1) == '-')
                    || (expression.charAt(i + 1) == '/') || (expression.charAt(i + 1) == '*'))) {
                f = false;
                break;
            }
			
			if(expression.charAt(i)==','){
                f = false;
                break;}


            if( (expression.charAt(i) == '/') &&  (expression.charAt(i + 1) == '0')){
                f=false;
                break;
            }
            if(i!=expression.length()-1) {
                if ((expression.charAt(i) == ')') && (expression.charAt(i + 1) >= '0' && expression.charAt(i + 1) <= '9')) {
                    f = false;
                    break;
                }
            }
            if(i!=expression.length()-1) {
                if ((expression.charAt(i) >= '0' && expression.charAt(i) <= '9') && (expression.charAt(i + 1) == '(')) {
                    f = false;
                    break;
                }
                else
                if( (!(expression.charAt(i+1)>='0' && expression.charAt(i+1)<='9')) && (expression.charAt(i)=='.'))
                {
                    f=false;
                    break;
                }
            }

        }

        return f;
        }

  public double whichOperator(String op,String oper1,String oper2){

        double d1= Double.parseDouble(oper1);
        double d2=Double.parseDouble(oper2);
        double rez=0;

        if(op.equals("*")) {
            rez=d2 * d1;
       }
        if (op.equals("/")) {
			if(d1!=0){
                rez = d2 / d1;
			}
			else
				return 0;
        }

        if(op.equals("+")) {
             rez=d2 + d1;
        }

       if (op.equals("-")) {
            rez=d2 - d1;
       }
     return rez;
   }
   
         public boolean isInteger(String s) {
        for(int i=0;i<s.length();i++)
            if(s.charAt(i)=='.' && s.charAt(i+1)!='0')
                return false;
        return true;
    }

    public String parse(String expression)  {
          double r=0; 
        int countcl=0;
        int countop=0;		
	   stackOperations.clear();
        stackRPN.clear();
	try {
		if (expression.equals(null)) {
			return expression;
		}
		if (expression.trim().length() == 0) {
			return null;
		}
    // делаю замены в выражении
		expression = expression.replace(" ", "").replace("(-", "(0-")
				.replace(",-", ",0-");
		if (expression.charAt(0) == '-') {
			expression = "0" + expression;
		}

		boolean flag = checker(expression);
		if (!flag) {
			return null;
		}

		// разделяю входную строку на токены
		StringTokenizer stringTokenizer = new StringTokenizer(expression, "+" + "-" + "*" + "/" + "(" + ")" + SEPARATOR, true);

		while (stringTokenizer.hasMoreTokens()) {                //обработка
			String token = stringTokenizer.nextToken();
			if (isSeparator(token)) {
				while (!stackOperations.empty() && !isOpenBracket(stackOperations.lastElement())) { //пока стек операторов не пуст и последний элемент в нем не явл-ся открыв скобкой
					stackRPN.push(stackOperations.pop());
				}
			} else if (isOpenBracket(token)) {
				countop++;
				stackOperations.push(token);
			} else if (isCloseBracket(token)) {
				while (!stackOperations.empty() && !isOpenBracket(stackOperations.lastElement())) // пока стек операторов не пуст и последний элеимент в нем не откры скобка
				{
					stackRPN.push(stackOperations.pop());
					countcl++;
				}
				if (!stackOperations.empty()) {
					stackRPN.push(stackOperations.pop());
				}
			} else if (isNumber(token)) {
				stackRPN.push(token);
			} else if (isOperator(token)) {
				while (!stackOperations.empty() && isOperator(stackOperations.lastElement())
						&& getPrecedence(token) <= getPrecedence(stackOperations.lastElement())) {
					stackRPN.push(stackOperations.pop());
				}
				stackOperations.push(token);
			}
		}

		while (!stackOperations.empty()) {
			stackRPN.push(stackOperations.pop());
		}

		System.out.println("RPN:" + stackRPN);
		Collections.reverse(stackRPN);
		while (!stackRPN.empty()) {
			String str = stackRPN.pop();
			if (isNumber(str)) {
				stackRez.push(str);
			}
			System.out.println("Rez" + stackRez);
			if (isOperator(str)) {
				r = whichOperator(str, stackRez.pop(), stackRez.pop());
				stackRez.push(Double.toString(r));
			}
		}	
		if (countcl != countop) {
			return null;
		}
		if (r == 0) {
			return null;
		}
		boolean vir;
		stackAnswer.push(stackRez.pop());
		String str = stackAnswer.pop();
		vir = isInteger(str);
		if (vir)
			str = str.substring(0, str.length() - 2);
	return str;
	} catch (NullPointerException e){
		return null;
	}
	}
	

    public String evaluate(String statement) {
        return parse(statement);
    }
}